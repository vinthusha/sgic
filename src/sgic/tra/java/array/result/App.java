package sgic.tra.java.array.result;

import sgic.tra.java.array.Subject;
import sgic.tra.java.array.imple.AlResultServiceImp;
import sgic.tra.java.array.imple.SubjectServiceImpl;
import sgic.tra.java.array.service.AlResultService;
import sgic.tra.java.array.service.SubjectService;

public class App {
	
public static void main(String[] args) {
	
	
	SubjectService subjectStore = new SubjectServiceImpl();
	
	subjectStore.addSubject(new Subject("C001", "English"));
	subjectStore.addSubject(new Subject("C002", "Apptitude"));
	subjectStore.addSubject(new Subject("S001", "CombainedMaths"));
	subjectStore.addSubject(new Subject("S002", "Physics"));
	subjectStore.addSubject(new Subject("S003", "Chemistry"));
	subjectStore.addSubject(new Subject("S004", "ICT"));
	
	AlResultService alRes = new AlResultServiceImp(subjectStore);
	alRes.assignIndexNumber("IN001");
	alRes.setAppitudeMarks("C001", 80.0);
	alRes.setEnglishMarks("C002", 50.0);
	alRes.addMarks("S001", 40.0);
	alRes.addMarks("S002", 55.0);
	alRes.addMarks("S003", 65.0);

	alRes.printResult();
	
	AlResultService alRes1 = new AlResultServiceImp(subjectStore);
	alRes1.assignIndexNumber("IN001");
	alRes1.setAppitudeMarks("C001", 60.0);
	alRes1.setEnglishMarks("C002", 70.0);
	alRes1.addMarks("S001", 50.0);
	alRes1.addMarks("S002", 65.0);
	alRes1.addMarks("S003", 85.0);

	alRes1.printResult();	
}
}
