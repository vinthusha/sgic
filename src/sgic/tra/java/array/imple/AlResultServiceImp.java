package sgic.tra.java.array.imple;

import sgic.tra.java.array.Subject;
import sgic.tra.java.array.service.AlResultService;
import sgic.tra.java.array.service.SubjectService;

public class AlResultServiceImp implements AlResultService {

	private String indexNumber;
	private SubjectService subjectService;

	private Subject[] subjectsSubject = new Subject[5];
	private double[] studentMarks = new double[5];

	private int counter = 2;

	public AlResultServiceImp(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Override
	public void assignIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;

	}

	@Override
	public void addMarks(String subjectId, double marks) {
		Subject subjectObj = subjectService.getSubjectById(subjectId);
		subjectsSubject[counter]=subjectObj;
		studentMarks[counter] = marks;
		counter++;
	}

	@Override
	public double getTotal() {
		double total = 0;
		if (counter == 5) {
			for (int i = 2; i < counter; i++) {
				total = total + studentMarks[i];

			}
		}
		return total;
	}

	@Override
	public double getAverage() {
		
		return getTotal()/3;
	}

	@Override
	public double getStandardDeviation() {
		return counter;
		
		
	}

	@Override
	public void printResult() {
		if (counter == 5) {
		System.out.println("--------------ResultSheet----------------");
		System.out.println("IndexNumber : " +this.indexNumber);
		System.out.println("Subject ID\t|\tMarks");
		
		for (int i = 0; i < counter; i++) {
				System.out.println(subjectsSubject[i].getId() + "\t|\t" + subjectsSubject[i].getName() + "\t|\t"
						+ studentMarks[i]);
		}
		System.out.println("Total :" + this.getTotal());
		System.out.println("Average :" + this.getAverage());
		System.out.println("StandaraDeviation :" + this.getStandardDeviation());
		}
	}

	@Override
	public void setEnglishMarks(String subjectId, double marks) {
		Subject subObj = subjectService.getSubjectById(subjectId);
		subjectsSubject[0] = subObj;
		studentMarks[0] = marks;

	}

	@Override
	public void setAppitudeMarks(String subjectId, double marks) {
		Subject subObj = subjectService.getSubjectById(subjectId);
		subjectsSubject[1] = subObj;
		studentMarks[1] = marks;

	}

}
