package sgic.tra.java.array.service;

public interface AlResultService {
	
	public void assignIndexNumber(String indexNumber);
	public void addMarks(String subjectId,double marks);
	public double getTotal();
	public double getAverage();
	public double getStandardDeviation();
	public void setEnglishMarks(String subjectId,double marks);
	public void setAppitudeMarks(String subjectId,double marks);
	public void printResult();
	

}
