package sgic.tra.java.array.service;

import sgic.tra.java.array.Subject;

public interface SubjectService {
	public void addSubject(Subject subject);
	public Subject getSubjectById(String subject);
}
